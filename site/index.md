---
layout: default
---
# Introduction

This project is to create some programs which reproduce the problems of the vulnerabilities
[Meltdown](https://meltdownattack.com/) and [Spectre](https://spectreattack.com/).

It contains some programs that try to reproduce the vulnerabilities described.

All programs are designed to run under Linux x86-64.

The main documentation is in the file
[Meltdown-Spectre.pdf](Meltdown-Spectre.pdf) which is the result of
[literate programming](https://en.wikipedia.org/wiki/Literate_programming). The source for this is the file
[Meltdown-Spectre.nw](https://git.sw4j.net/osdevelopment-info/meltdown/blob/master/Meltdown-Spectre.nw) (mirrored to
[GitLab](https://git.sw4j.net/osdevelopment-info/meltdown/blob/master/Meltdown-Spectre.nw)).

# Work in Progess

This is a work in progress therefore not all source code gives the expected results.

The following source codes are working as intended

* [cachetiming.asm](asm/cachetiming.asm)
* [cachereadbyte.asm](asm/cachereadbyte.asm)
* [cachereadbyte2.asm](asm/cachereadbyte2.asm)
* [cachereadbyte3.asm](asm/cachereadbyte3.asm)
* [cacheread.asm](asm/cacheread.asm)

If you want to generate the programs and documentation yourself go to [Full Build](#full-build).

# Downloading executables

The binaries of the latest build are also available here:

* [cachetiming](bin/cachetiming)
* [cachereadbyte](bin/cachereadbyte)
* [cachereadbyte2](bin/cachereadbyte2)
* [cachereadbyte3](bin/cachereadbyte3)
* [cacheread](bin/cacheread)

A [short description](./using.html) of the programs is also available.

# Building Instructions

## Build Samples

For building the samples you need the following packages installed:

* build-essential (for `ld`)
* make
* nasm

Next you have to decide, either [download](#download-repository) or [clone](#clone-repository) the repository or get the
[latest sources](#build-from-latest-sources).

### Download Repository

You can download the latest repository in various archive formats:

* [zip](https://git.sw4j.net/osdevelopment-info/meltdown/-/archive/master/meltdown-master.zip)
* [tar](https://git.sw4j.net/osdevelopment-info/meltdown/-/archive/master/meltdown-master.tar)
* [tar.gz](https://git.sw4j.net/osdevelopment-info/meltdown/-/archive/master/meltdown-master.tar.gz)
* [tar.bz2](https://git.sw4j.net/osdevelopment-info/meltdown/-/archive/master/meltdown-master.tar.bz2)

Alternatively from the mirror:

* [zip](https://gitlab.com/osdevelopment-info/meltdown/-/archive/master/meltdown-master.zip)
* [tar](https://gitlab.com/osdevelopment-info/meltdown/-/archive/master/meltdown-master.tar)
* [tar.gz](https://gitlab.com/osdevelopment-info/meltdown/-/archive/master/meltdown-master.tar.gz)
* [tar.bz2](https://gitlab.com/osdevelopment-info/meltdown/-/archive/master/meltdown-master.tar.bz2)

Then you can unpack the downloaded archive. After that continue with [Building Repository](#building-repository).

### Clone Repository

You can clone the repository with `git` (which must have been installed before)

```
git clone https://git.sw4j.net/osdevelopment-info/meltdown.git
```

or from the mirror

```
git clone https://gitlab.com/osdevelopment-info/meltdown.git
```

Next continue with [Building Repository](#building-repository).

### Building Repository

As prerequisites for a successful build of the samples you need the following packages

* build-essential (for `ld`)
* make
* nasm
* noweb

After extracting or cloning the repository go into the folder `asm/` in the repository and execute

```
make
```

You can then find the created executables in `bin/` in the repository.

### Build from Latest Sources

As prerequisites for a successful build of the samples you need the following packages

* build-essential (for `ld`)
* nasm

You can download the latest `.asm` files as an archive:

* [asm.zip](asm.zip)
* [asm.tar](asm.tar)
* [asm.tar.gz](asm.tar.gz)
* [asm.tar.bz2](asm.tar.bz2)

Go on with [Build Single .asm File](#build-single-asm-file).

### Build Single .asm File

When you have the `.asm` file you can build an executable from an `.asm` file by executing

```
nasm -f elf64 -g -F stabs <file>.asm -o <file>.o
ld -melf_x86_64 -o <file> <file>.o
```

Now you have an executable `<file>` which you can execute.

## Full Build

As prerequisites for a successful complete usage you need the following packages

* git
* build-essential (for `ld`)
* make
* nasm
* texlive-full (for the complete documentation)
* noweb

If you cannot install all packages (e.g. in CentOS) then you can alternatively use a [docker](https://docker.io)
container and use this for building. The docker image names are

* For noweb
  * registry.gitlab.com/sw4j-net/noweb/master:latest
  * registry.sw4j.net/sw4j-net/noweb/master:latest
* For nasm
  * registry.gitlab.com/sw4j-net/nasm/master:latest
  * registry.sw4j.net/sw4j-net/nasm/master:latest

To see how to build please refer to the
[CI build file](https://git.sw4j.net/osdevelopment-info/meltdown/blob/master/.gitlab-ci.yml) or
[its mirror](https://gitlab.com/osdevelopment-info/meltdown/blob/master/.gitlab-ci.yml).

After creating your environment you can clone the repository with

```
git clone https://git.sw4j.net/osdevelopment-info/meltdown.git
```

or mirrored at

```
git clone https://gitlab.com/osdevelopment-info/meltdown.git
```

In the repository folder you can now execute the following command to create the executables (placed in `bin/`)

```
make
```

To build the complete documentation as pdf file execute

```
make pdf
```
