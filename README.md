[![pipeline status](https://git.sw4j.net/osdevelopment-info/meltdown/badges/master/pipeline.svg)](https://git.sw4j.net/osdevelopment-info/meltdown/pipelines)

# Introduction

This project is to create some programs which reproduce the problems of the vulnerabilities
[Meltdown](https://meltdownattack.com/) and [Spectre](https://spectreattack.com/).

Further informations on building can be found on the [project page](https://osdevelopment-info.pages.sw4j.net/meltdown/).
